/*
 *  linux/drivers/video/ps3rsx/ps3rsx.c -- PS3 GPU driver
 *  	Copyright (C) 2009 Kristian Jerpetj�n <kristian.jerpetjoen@gmail.com>
 *
 *  This file is based on
 *
 *  linux/drivers/video/ps3fb.c -- PS3 GPU frame buffer device
 *
 *	Copyright (C) 2006 Sony Computer Entertainment Inc.
 *	Copyright 2006, 2007 Sony Corporation
 *
 *  This file is based on :
 *
 *  linux/drivers/video/vfb.c -- Virtual frame buffer device
 *
 *	Copyright (C) 2002 James Simmons
 *
 *	Copyright (C) 1997 Geert Uytterhoeven
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of this archive for
 *  more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/init.h>
//#include <linux/pci.h>
#include <linux/console.h>
#include <linux/backlight.h>
#ifdef CONFIG_MTRR
#include <asm/mtrr.h>
#endif
#ifdef CONFIG_PPC_OF
#include <asm/prom.h>
//#include <asm/pci-bridge.h>
#endif
#ifdef CONFIG_BOOTX_TEXT
#include <asm/btext.h>
#endif


static int __init ps3rsxfb_setup(void)
{

}

static int __init ps3rsxfb_init (void)
{
	ps3rsxfb_setup();
}

static void __exit ps3rsxfb_exit(void)
{

}

module_init(ps3rsxfb_init);
module_exit(ps3rsxfb_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Next Gen PS3 GPU Frame Buffer Driver");
MODULE_AUTHOR("Kristian Jerpetj�n");
MODULE_ALIAS(PS3_MODULE_ALIAS_GPU);