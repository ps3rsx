/*
 *  linux/drivers/video/ps3rsx/rsx_setup.c -- PS3 GPU driver
 *  	Copyright (C) 2009 Kristian Jerpetj�n <kristian.jerpetjoen@gmail.com>
 *
 *  This file is based on
 *
 *  linux/drivers/video/ps3fb.c -- PS3 GPU frame buffer device
 *
 *	Copyright (C) 2006 Sony Computer Entertainment Inc.
 *	Copyright 2006, 2007 Sony Corporation
 *
 *  This file is based on :
 *
 *  linux/drivers/video/vfb.c -- Virtual frame buffer device
 *
 *	Copyright (C) 2002 James Simmons
 *
 *	Copyright (C) 1997 Geert Uytterhoeven
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of this archive for
 *  more details.
 */



static const struct fb_videomode ps3fb_modedb[] = {
	/* 60 Hz broadcast modes (modes "1" to "5") */
	    {
		    /* 480i */
		    "480i", 60, 576, 384, 74074, 130, 89, 78, 57, 63, 6,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
	    },    {
		    /* 480p */
		    "480p", 60, 576, 384, 37037, 130, 89, 78, 57, 63, 6,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
	    },    {
		    /* 720p */
		    "720p", 60, 1124, 644, 13481, 298, 148, 57, 44, 80, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
	    },    {
		    /* 1080i */
		    "1080i", 60, 1688, 964, 13481, 264, 160, 94, 62, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
	    },    {
		    /* 1080p */
		    "1080p", 60, 1688, 964, 6741, 264, 160, 94, 62, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
	    },

     /* 50 Hz broadcast modes (modes "6" to "10") */
     {
	     /* 576i */
	     "576i", 50, 576, 460, 74074, 142, 83, 97, 63, 63, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     },    {
	     /* 576p */
	     "576p", 50, 576, 460, 37037, 142, 83, 97, 63, 63, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     },    {
	     /* 720p */
	     "720p", 50, 1124, 644, 13468, 298, 478, 57, 44, 80, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     },    {
	     /* 1080i */
	     "1080i", 50, 1688, 964, 13468, 264, 600, 94, 62, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     },    {
	     /* 1080p */
	     "1080p", 50, 1688, 964, 6734, 264, 600, 94, 62, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     },

     [FIRST_NATIVE_MODE_INDEX] =
     /* 60 Hz broadcast modes (full resolution versions of modes "1" to "5") */
     {
	     /* 480if */
	     "480if", 60, 720, 480, 74074, 58, 17, 30, 9, 63, 6,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     }, {
	     /* 480pf */
	     "480pf", 60, 720, 480, 37037, 58, 17, 30, 9, 63, 6,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     }, {
	     /* 720pf */
	     "720pf", 60, 1280, 720, 13481, 220, 70, 19, 6, 80, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     }, {
	     /* 1080if */
	     "1080if", 60, 1920, 1080, 13481, 148, 44, 36, 4, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     }, {
	     /* 1080pf */
	     "1080pf", 60, 1920, 1080, 6741, 148, 44, 36, 4, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     },

     /* 50 Hz broadcast modes (full resolution versions of modes "6" to "10") */
     {
	     /* 576if */
	     "576if", 50, 720, 576, 74074, 70, 11, 39, 5, 63, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     }, {
	     /* 576pf */
	     "576pf", 50, 720, 576, 37037, 70, 11, 39, 5, 63, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     }, {
	     /* 720pf */
	     "720pf", 50, 1280, 720, 13468, 220, 400, 19, 6, 80, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     }, {
	     /* 1080if */
	     "1080if", 50, 1920, 1080, 13468, 148, 484, 36, 4, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_INTERLACED
     }, {
	     /* 1080pf */
	     "1080pf", 50, 1920, 1080, 6734, 148, 484, 36, 4, 88, 5,
      FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED
     },

     /* VESA modes (modes "11" to "13") */
     {
	     /* WXGA */
	     "wxga", 60, 1280, 768, 12924, 160, 24, 29, 3, 136, 6,
      0, FB_VMODE_NONINTERLACED,
      FB_MODE_IS_VESA
     }, {
	      /* SXGA */
	      "sxga", 60, 1280, 1024, 9259, 248, 48, 38, 1, 112, 3,
       FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT, FB_VMODE_NONINTERLACED,
       FB_MODE_IS_VESA
     }, {
	       /* WUXGA */
	       "wuxga", 60, 1920, 1200, 6494, 80, 48, 26, 3, 32, 6,
	FB_SYNC_HOR_HIGH_ACT, FB_VMODE_NONINTERLACED, FB_MODE_IS_VESA
     }
};